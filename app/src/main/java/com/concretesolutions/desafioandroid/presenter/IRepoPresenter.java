package com.concretesolutions.desafioandroid.presenter;

import android.app.Activity;

public interface IRepoPresenter {

    Activity getLocalActivity();
    void downloadRepositories(int page);

    void downloadPulls(String owner, String repository);

    void showNextView();

    boolean onBackPressed();

    void openBrowser(String url);

    void back();

    void setTitle(String title);
}
