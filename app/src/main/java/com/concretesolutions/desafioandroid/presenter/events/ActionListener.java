package com.concretesolutions.desafioandroid.presenter.events;

import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import com.concretesolutions.desafioandroid.pojo.JsonPojo;
import com.concretesolutions.desafioandroid.presenter.IRepoPresenter;
import com.concretesolutions.desafioandroid.view.components.EndlessScrollListener;

public class ActionListener {

    public static class ButtonsOnClickActionListener implements OnClickListener {
        private final IRepoPresenter presenter;
        public ButtonsOnClickActionListener(final IRepoPresenter presenter) {
            this.presenter = presenter;
        }

        @Override
        public void onClick(final View view) {
            this.presenter.back();
        }
    }

    public static class ButtonsItemOnClickActionListener implements OnClickListener {
        private final IRepoPresenter presenter;
        private final JsonPojo JsonPojo;
        public ButtonsItemOnClickActionListener(final IRepoPresenter presenter, JsonPojo JsonPojo, int position) {
            this.presenter            = presenter;
            this.JsonPojo = JsonPojo;
        }

        @Override
        public void onClick(final View view) {
            this.presenter.setTitle(this.JsonPojo.getName());
            this.presenter.showNextView();
            this.presenter.downloadPulls(this.JsonPojo.getOwner().getLogin(),this.JsonPojo.getName());
        }
    }

    public static class ButtonsPullItemOnClickActionListener implements OnClickListener {
        private final IRepoPresenter presenter;
        private final  String url;
        public ButtonsPullItemOnClickActionListener(final IRepoPresenter presenter, String url) {
            this.presenter            = presenter;
            this.url   = url;
        }

        @Override
        public void onClick(final View view) {
            this.presenter.openBrowser(this.url);
        }
    }

    public static class EndlessScrollActionListener extends EndlessScrollListener {
        private final transient IRepoPresenter presenter;

        public EndlessScrollActionListener(final IRepoPresenter presenter) {
            this.presenter = presenter;
        }


        @Override
        public boolean onLoadMore(int page, int totalItemsCount) {
            Log.i("challenge", "onLoadMore:page -" + page+" - totalItemsCount:"+totalItemsCount);
            // Triggered only when new data needs to be appended to the list
            // Add whatever code is needed to append new items to your AdapterView
            this.presenter.downloadRepositories(page);
            // or customLoadMoreDataFromApi(totalItemsCount);
            return true; // ONLY if more data is actually being loaded; false otherwise.
        }

        // Append more data into the adapter
        public void customLoadMoreDataFromApi(int offset) {
            // This method probably sends out a network request and appends new data items to your adapter.
            // Use the offset value and add it as a parameter to your API request to retrieve paginated data.
            // Deserialize API response and then construct new objects to append to the adapter
        }
    }
}
