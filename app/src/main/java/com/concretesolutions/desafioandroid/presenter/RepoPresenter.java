package com.concretesolutions.desafioandroid.presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.concretesolutions.desafioandroid.controller.MainActivity;
import com.concretesolutions.desafioandroid.R;
import com.concretesolutions.desafioandroid.pojo.JsonPull;
import com.concretesolutions.desafioandroid.pojo.JsonPojo;
import com.concretesolutions.desafioandroid.service.ServiceGit;
import com.concretesolutions.desafioandroid.view.RepoView;
import com.concretesolutions.desafioandroid.view.adapter.PullAdapter;
import com.concretesolutions.desafioandroid.view.adapter.RepoAdapter;
import com.concretesolutions.desafioandroid.presenter.events.ActionListener;
import com.concretesolutions.desafioandroid.view.interfaces.IRepoView;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class RepoPresenter extends Fragment implements IRepoPresenter {
    private static final String IS_DETAIL = "IS_DETAIL";
    private static final String TITLE = "TITLE";
    private static final String PULL_REQUEST_TITLE = "PULL_REQUEST_TITLE";
    private FragmentActivity activity;
    private RepoAdapter repoAdapter;
    private int page = 1;
    private IRepoView repositoryView;
    private PullAdapter pullAdapter;
    private AsyncTask<String, String, String> repositoryRequestTask;
    private AsyncTask<String, String, String> pullRequestTask;
    private String title;
    private String pullRequestTitle;
    private String API = "https://api.github.com";

    @Override
    public void onAttach(Activity activity) {
        if (activity instanceof FragmentActivity) {
            this.activity = (FragmentActivity) activity;
        }
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i("desafio", "desafio.savedInstanceState - " + savedInstanceState);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle bundle) {
        final ViewGroup view = (ViewGroup) inflater.inflate(R.layout.app_repository, container, false);
        this.createView(view);
        setRetainInstance(true);
        return view;
    }

    private void createView(final ViewGroup view) {
        repositoryView = new RepoView(view);
        if(repoAdapter ==null){
            this.repositoryView.showProgressBar(View.VISIBLE);
            repoAdapter = new RepoAdapter(this);
            this.downloadRepositories(this.page);
        }

        repositoryView.setRepositoryAdapter(repoAdapter);
        repositoryView.setEndlessScrollListener(new ActionListener.EndlessScrollActionListener(this));
        repositoryView.setOnClickListener(new ActionListener.ButtonsOnClickActionListener(this));
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



        if(savedInstanceState!=null){
           if(savedInstanceState.getBoolean(IS_DETAIL)&&pullAdapter!=null&&savedInstanceState.getString(TITLE)!=null&&savedInstanceState.getString(PULL_REQUEST_TITLE)!=null){
               this.repositoryView.setTitle(savedInstanceState.getString(TITLE));
               this.repositoryView.setPullTitle(savedInstanceState.getString(PULL_REQUEST_TITLE));
               repositoryView.setPullAdapter(pullAdapter);
               this.repositoryView.nextView();
           }
        }
    }

    @Override
    public FragmentActivity getLocalActivity(){
        return  this.getActivity();
    }

    @Override
    public void downloadRepositories(int page) {
        this.page = page;
        if(this.page>1){
            this.repositoryView.showProgressBar(View.VISIBLE);
        }

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ServiceGit git = retrofit.create(ServiceGit.class);
        //https://api.github.com/search/repositories?q=language:Java&sort=stars&page=" + page
        Call<JsonPojo> call = git.getRepository("language:Java", "stars", String.valueOf(page));
        call.enqueue(new Callback<JsonPojo>() {
            @Override
            public void onResponse(Call<JsonPojo> call, Response<JsonPojo> response) {
                JsonPojo sucessJSON = response.body();
                if (sucessJSON == null) {
                    //404 or the response cannot be converted to JSONPullScheme.
                    ResponseBody responseBody = response.errorBody();
                    if (responseBody != null) {
                        Toast.makeText(getContext(), "Body:" + responseBody, Toast.LENGTH_SHORT).show();
                        try {
                            Toast.makeText(getContext(), "responseBody = " + responseBody.string(), Toast.LENGTH_SHORT).show();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(getContext(), "responseBody = " + responseBody, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (sucessJSON != null && sucessJSON.getItems() != null) {
                        repoAdapter.update(sucessJSON.getItems());
                        Log.i("desafio", "desafio.onSuccess - " + String.valueOf(sucessJSON.getTotalCount()));
                    }
                }
                repositoryView.showProgressBar(View.GONE);
            }

            @Override
            public void onFailure(Call<JsonPojo> call, Throwable t) {
                repositoryView.showProgressBar(View.GONE);
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }

        });
//        repositoryRequestTask = new RequestTask(new RepositoryActionListener.JSONRepositoyCallBack(this)).execute("https://api.github.com/search/repositories?q=language:Java&sort=stars&page=" + page);
    }

    @Override
    public void downloadPulls(String owner, String repository) {
        this.repositoryView.showProgressBar(View.VISIBLE);
        this.repositoryView.setPullTitle("");
        this.pullAdapter = new PullAdapter(this);
        repositoryView.setPullAdapter(pullAdapter);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ServiceGit git = retrofit.create(ServiceGit.class);
        Call<List<JsonPull>> call = git.getPull(owner,repository);
        call.enqueue(new Callback<List<JsonPull>>() {
            @Override
            public void onResponse(Call<List<JsonPull>> call, Response<List<JsonPull>> response) {
                List<JsonPull> sucessJSON = response.body();
                if (sucessJSON == null) {
                    //404 or the response cannot be converted to JSONPullScheme.
                    ResponseBody responseBody = response.errorBody();
                    if (responseBody != null) {
                        Toast.makeText(getContext(), "Body:" + responseBody, Toast.LENGTH_SHORT).show();
                        try {
                            Toast.makeText(getContext(), "responseBody = " + responseBody.string(), Toast.LENGTH_SHORT).show();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(getContext(), "responseBody = " + responseBody, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (sucessJSON != null && pullAdapter != null) {
                        pullRequestTitle = "Pull Request Count: " + sucessJSON.size();
                        repositoryView.setPullTitle(pullRequestTitle);
                        pullAdapter.update(sucessJSON);
                    }
                }
                repositoryView.showProgressBar(View.GONE);
            }

            @Override
            public void onFailure(Call<List<JsonPull>> call, Throwable t) {
                repositoryView.showProgressBar(View.GONE);
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }

        });
    }

    @Override
    public void showNextView() {
        this.repositoryView.showProgressBar(View.GONE);
        this.repositoryView.nextView();
    }

    @Override
    public void openBrowser(String url) {
        final Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    @Override
    public void back() {
        this.repositoryView.setTitle(getString(R.string.title_repository));
        if(this.getLocalActivity() instanceof MainActivity){
            ((MainActivity) this.getLocalActivity()).onBackPressed();
        }
    }

    @Override
    public boolean onBackPressed() {
        this.repositoryView.setTitle(getString(R.string.title_repository));
        if(this.repositoryView.isDetail()){
            this.repositoryView.nextView();
            return false;
        }else{
            return true;
        }
    }

    @Override
    public Context getContext() {
        return super.getContext();
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
        this.repositoryView.setTitle(title);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(IS_DETAIL, this.repositoryView.isDetail());
        outState.putString(TITLE, this.title);
        outState.putString(PULL_REQUEST_TITLE, this.pullRequestTitle);
        super.onSaveInstanceState(outState);
    }
}