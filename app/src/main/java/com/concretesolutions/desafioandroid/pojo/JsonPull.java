package com.concretesolutions.desafioandroid.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class JsonPull {

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("body")
    @Expose
    private String body;

    @SerializedName("created_at")
    @Expose
    private String createdAt;

    @SerializedName("html_url")
    @Expose
    private String htmlUrl;

    @SerializedName("user")
    @Expose
    private Owner user;


    /**
     *
     * @return
     *     The htmlUrl
     */
    public String getHtmlUrl() {
        return htmlUrl;
    }

    /**
     *
     * @param htmlUrl
     *     The html_url
     */
    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    /**
     *
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     *
     * @return
     *     The user
     */
    public Owner getUser() {
        return user;
    }

    /**
     *
     * @param user
     *     The user
     */
    public void setUser(Owner user) {
        this.user = user;
    }

    /**
     *
     * @return
     *     The body
     */
    public String getBody() {
        return body;
    }

    /**
     *
     * @param body
     *     The body
     */
    public void setBody(String body) {
        this.body = body;
    }

    /**
     *
     * @return
     *     The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     *     The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

}
