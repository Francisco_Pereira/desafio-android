package com.concretesolutions.desafioandroid.controller;


import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

import com.concretesolutions.desafioandroid.R;
import com.concretesolutions.desafioandroid.presenter.IRepoPresenter;
import com.concretesolutions.desafioandroid.presenter.RepoPresenter;

public class MainActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_controller);
        if (savedInstanceState == null) {
            final RepoPresenter fragment =  new RepoPresenter();
            this.getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_NONE).add(R.id.content, fragment, "RepositoryPresenter").commitAllowingStateLoss();
        }
    }

    @Override
    public void onBackPressed() {
        final RepoPresenter fragment =  new RepoPresenter();
        if(this.getSupportFragmentManager().getFragments().size()>0){
             if(this.getSupportFragmentManager().getFragments().get(0) instanceof IRepoPresenter){
                 IRepoPresenter repositoryPresenter  = (IRepoPresenter) this.getSupportFragmentManager().getFragments().get(0);
                 if (repositoryPresenter.onBackPressed()) {
                     super.onBackPressed();
                 }
             }else{
                 super.onBackPressed();
             }
        }else{
            super.onBackPressed();
        }
    }
}
