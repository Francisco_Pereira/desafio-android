package com.concretesolutions.desafioandroid.service;

import com.concretesolutions.desafioandroid.pojo.JsonPull;
import com.concretesolutions.desafioandroid.pojo.JsonPojo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface ServiceGit {

    @GET("/search/repositories")
    Call<JsonPojo> getRepository(@Query("q") String q, @Query("sort") String sort, @Query("page") String page);

    @GET("/repos/{owner}/{repository}/pulls")
    Call<List<JsonPull>> getPull(@Path("owner") String owner,
                                 @Path("repository") String repo);
}
