package com.concretesolutions.desafioandroid.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.concretesolutions.desafioandroid.R;
import com.concretesolutions.desafioandroid.pojo.JsonPojo;
import com.concretesolutions.desafioandroid.presenter.IRepoPresenter;
import com.concretesolutions.desafioandroid.presenter.events.ActionListener;
import com.concretesolutions.desafioandroid.view.RepoAdapterView;
import com.concretesolutions.desafioandroid.view.interfaces.IRepoAdapterView;

import java.util.ArrayList;
import java.util.List;

public class RepoAdapter extends BaseAdapter {

    private transient LayoutInflater inflater;
    private transient IRepoPresenter presenter;
    private transient List<JsonPojo> repositoryList;

    public RepoAdapter(IRepoPresenter presenter) {
        this.presenter = presenter;
        this.inflater = LayoutInflater.from(presenter.getLocalActivity());
        this.repositoryList = new ArrayList<JsonPojo>();
    }

    @Override
    public int getCount() {
        return this.repositoryList.size();
    }

    @Override
    public JsonPojo getItem(int position) {
        return this.repositoryList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        IRepoAdapterView repositoryItemView = null;
        if (convertView == null) {
            convertView =  inflater.inflate(R.layout.app_repository_item, null);
            repositoryItemView = new RepoAdapterView((ViewGroup) convertView);
            convertView.setTag(repositoryItemView);
        } else {
            repositoryItemView = (IRepoAdapterView) convertView.getTag();
        }

        repositoryItemView.setName(this.getItem(position).getName());
        repositoryItemView.setDescription(this.getItem(position).getDescription());
        repositoryItemView.setForkCount(String.valueOf(this.getItem(position).getForksCount()));
        repositoryItemView.setStarCount(String.valueOf(this.getItem(position).getStargazersCount()));
        repositoryItemView.setUserName(this.getItem(position).getOwner().getLogin());
        repositoryItemView.setPhoto(this.getItem(position).getOwner().getAvatarUrl());
        repositoryItemView.setButtonOnclickListener(new ActionListener.ButtonsItemOnClickActionListener(presenter, getItem(position),position));
        return convertView;
    }

    public void update(List<JsonPojo> repositoryList){
        this.repositoryList.addAll(repositoryList);
        this.notifyDataSetChanged();
    }

}
