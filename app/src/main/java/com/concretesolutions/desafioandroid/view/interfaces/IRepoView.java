package com.concretesolutions.desafioandroid.view.interfaces;

import android.view.View;
import android.widget.BaseAdapter;

import com.concretesolutions.desafioandroid.presenter.events.ActionListener;

public interface IRepoView {
    void setPullTitle(String title);

    void setTitle(String title);

    void nextView();

     boolean isDetail();

    void setRepositoryAdapter(BaseAdapter adapter);

    void setPullAdapter(BaseAdapter adapter);

    void setEndlessScrollListener(ActionListener.EndlessScrollActionListener endlessScrollActionListener);

    void setOnClickListener(View.OnClickListener listener);

    void showProgressBar(int visibility);
}
