package com.concretesolutions.desafioandroid.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.concretesolutions.desafioandroid.R;
import com.concretesolutions.desafioandroid.pojo.JsonPull;
import com.concretesolutions.desafioandroid.presenter.IRepoPresenter;
import com.concretesolutions.desafioandroid.presenter.events.ActionListener;
import com.concretesolutions.desafioandroid.view.RepoAdapterView;
import com.concretesolutions.desafioandroid.view.interfaces.IRepoAdapterView;

import java.util.ArrayList;
import java.util.List;

public class PullAdapter extends BaseAdapter {
    private transient LayoutInflater inflater;
    private transient IRepoPresenter presenter;
    private transient List<JsonPull> pullList;

    public PullAdapter(IRepoPresenter presenter) {
        this.presenter = presenter;
        this.inflater = LayoutInflater.from(presenter.getLocalActivity());
        this.pullList = new ArrayList<JsonPull>();
    }

    @Override
    public int getCount() {
        return this.pullList.size();
    }

    @Override
    public JsonPull getItem(int position) {
        return this.pullList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        IRepoAdapterView repositoryItemView = null;
        if (convertView == null) {
            convertView =  inflater.inflate(R.layout.app_pull_item, null);
            repositoryItemView = new RepoAdapterView((ViewGroup) convertView);
            convertView.setTag(repositoryItemView);
        } else {
            repositoryItemView = (IRepoAdapterView) convertView.getTag();
        }
        repositoryItemView.setName(this.getItem(position).getTitle());
        repositoryItemView.setDescription(this.getItem(position).getBody() + "\n\n" + this.getItem(position).getCreatedAt());
        repositoryItemView.setUserName(this.getItem(position).getUser().getLogin());
        repositoryItemView.setPhoto(this.getItem(position).getUser().getAvatarUrl());
        repositoryItemView.setButtonOnclickListener(new ActionListener.ButtonsPullItemOnClickActionListener(presenter, this.getItem(position).getHtmlUrl()));
        return convertView;
    }

    public void update(List<JsonPull> repositoryList){
        this.pullList.addAll(repositoryList);
        this.notifyDataSetChanged();
    }

}
