package com.concretesolutions.desafioandroid.view;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.concretesolutions.desafioandroid.R;
import com.concretesolutions.desafioandroid.view.components.DeclareComponent;
import com.concretesolutions.desafioandroid.presenter.events.ActionListener;
import com.concretesolutions.desafioandroid.view.interfaces.IRepoView;

import java.lang.reflect.Field;

public class RepoView implements IRepoView {
    @DeclareComponent(componentId = R.id.pull_title)
    private TextView pullTitle;

    @DeclareComponent(componentId = R.id.title)
    private TextView title;

    @DeclareComponent(componentId = R.id.repository_list)
    private ListView repositoryList;

    @DeclareComponent(componentId = R.id.pull_list)
    private ListView pullList;

    @DeclareComponent(componentId = R.id.view_flipper)
    private ViewFlipper viewFlipper;

    @DeclareComponent(componentId = R.id.repository_progressbar_load)
    private View progressbarLoad;

    @DeclareComponent(componentId = R.id.back_btn)
    private View backBtn;

    public RepoView(final ViewGroup view) {
          declareComponentFromView(view, this);
    }

    @Override
    public void setPullTitle(String title){
        this.pullTitle.setText(title);
    }

    @Override
    public void setTitle(String title){
        this.title.setText(title);
    }

    @Override
    public void nextView(){
        this.viewFlipper.showNext();
    }

    @Override
    public boolean isDetail() {
        return this.viewFlipper.getDisplayedChild()==1;
    }

    @Override
    public void setRepositoryAdapter(BaseAdapter adapter){
        this.repositoryList.setAdapter(adapter);
    }

    @Override
    public void setPullAdapter(BaseAdapter adapter){
        this.pullList.setAdapter(adapter);
    }

    @Override
    public void setEndlessScrollListener(ActionListener.EndlessScrollActionListener endlessScrollActionListener) {
        this.repositoryList.setOnScrollListener(endlessScrollActionListener);
    }

    @Override
    public void setOnClickListener(View.OnClickListener listener) {
        this.backBtn.setOnClickListener(listener);
    }

    @Override
    public void showProgressBar(int visibility) {
        this.progressbarLoad.setVisibility(visibility);
    }

    public static void declareComponentFromView(final ViewGroup view, Object object) {
        Class clazz = object.getClass();
        for (Field field : clazz.getDeclaredFields()) {
            if (field.isAnnotationPresent(DeclareComponent.class)) {
                String fieldName = "";
                try {
                    DeclareComponent anotacao = field.getAnnotation(DeclareComponent.class);
                    if (anotacao.componentId() > 0) {
                        fieldName = object.getClass().getDeclaredField(field.getName()).toString();
                        Field fieldCurrent = object.getClass().getDeclaredField(field.getName());
                        fieldCurrent.setAccessible(true);
                        fieldCurrent.set(object, view.findViewById(anotacao.componentId()));
                    }
                } catch (NoSuchFieldException e) {
                    Log.i("AnotaçãoError","AnotaçãoError:" + e.getMessage());
                } catch (IllegalArgumentException e) {
                    Log.i("AnotaçãoError","AnotaçãoError:" + e.getMessage());
                } catch (IllegalAccessException e) {
                    Log.i("AnotaçãoError","AnotaçãoError:" + e.getMessage());
                }
            }
        }
    }
}

