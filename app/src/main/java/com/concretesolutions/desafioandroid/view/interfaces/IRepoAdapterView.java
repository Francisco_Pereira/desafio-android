package com.concretesolutions.desafioandroid.view.interfaces;

import android.view.View;

public interface IRepoAdapterView {
    void setName(String name);

    void setDescription(String description);

    void setForkCount(String forkCount);

    void setStarCount(String starCount);

    void setUserName(String userName);

    void setButtonOnclickListener(View.OnClickListener listener);

    void setPhoto(String url);
}
