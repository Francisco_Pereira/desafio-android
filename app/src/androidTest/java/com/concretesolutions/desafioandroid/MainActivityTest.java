package com.concretesolutions.desafioandroid;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.concretesolutions.desafioandroid.controller.MainActivity;

import net.vidageek.mirror.dsl.Mirror;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.runner.RunWith;

import java.io.IOException;

import okhttp3.mockwebserver.MockWebServer;


@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    private MockWebServer server;

    @Rule
    public ActivityTestRule<MainActivity>
    mActivityRule = new ActivityTestRule<>(MainActivity.class, false, false);

    @Before
    public void setUp() throws Exception{
        server = new MockWebServer();
        server.start();
        setupServerUrl();
    }

    @After
    public void teardown() throws IOException{
        server.shutdown();
    }

    private void setupServerUrl(){
        String url = server.url("/").toString();

        /*HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        final RepoPresenter usersApi = RepoPresenter();

        final JsonPojo api = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(RepoPresenter.GSON))
                .client(client)
                .build()
                .create(JsonPojo.class);

        setField(usersApi, "api", api);*/
    }

    private void setField(Object target, String fieldName, Object value) {
        new Mirror()
                .on(target)
                .set()
                .field(fieldName)
                .withValue(value);
    }
}
